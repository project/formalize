Formalize is a CSS and JS library to make forms look good. This Drupal
module provides integration.

For more details see http://formalize.me/